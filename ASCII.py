#1- Write a function to return the uppercase version of a letter
def toUpper(c):
    c = str(c)
    if (len(c) > 1 or
            ord(c) < 65 or
            (97 > ord(c) > 90) or
            ord(c) > 122
    ):
        return -1

    if 91 > ord(c) > 64:
        return c;

    return chr(ord(c) - 32)


#2- Write a function to return the lowercase version of a letter
def toLower(c):
    c = str(c)
    if (len(c) > 1 or
            ord(c) < 65 or
            (97 > ord(c) > 90) or
            ord(c) > 122
    ):
        return -1

    if 123 > ord(c) > 96:
        return c;

    return chr(ord(c) + 32)


#3- Write a function that returns true if the letter is an alphabet
def isAlphabet(c):
    c = str(c)
    if (len(c) > 1 or
            ord(c) < 65 or
            (97 > ord(c) > 90) or
            ord(c) > 122
    ):
        return False

    return True

#4- Write a function that returns true if an element is a digit
def isDigit(c):
    c = str(c)
    for char in str(c):
        if char == '-':
            continue
        if ord(char) < 48 or ord(char) > 57:
            return False

    return True

#5- Write a function to determine if the given character is a special character or not
def isSpecial(c):
    c = str(c)
    if len(c) > 1:
        return -1

    if (ord(c) < 48 or
            (65 > ord(c) > 57) or
            (97 > ord(c) > 90) or
            ord(c) > 122
    ):
        return True
    else:
        return False


if __name__ == "__main__":
    print(toLower('G')) # g
    print(toUpper('a')) # A

    print(isAlphabet('1')) # false
    print(isAlphabet(',')) # false
    print(isAlphabet('a')) # true

    print(isDigit(999)) # true
    print(isDigit('988')) # true
    print(isDigit('9a')) # false

    print(isSpecial(9)) # false
    print(isSpecial('A')) # false
    print(isSpecial(',')) # true
    print(isSpecial('@')) # true






